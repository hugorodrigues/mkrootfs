#!/bin/sh

#--
# Copyright (c) 2021 Hugo Rodrigues
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#++

mkdir -p dist

export VERBOSE=1

# load version and name
. src/variables.sh
# unload gpg
unset GNUPGHOME
rm -rf "${TEMPDIR}"
# load log
. src/log.sh

bin="dist/${MYNAME}"
tmp="dist/src.sh"

# Reset binary
{ echo "!/bin/sh"; echo ""; } > "${bin}" # hash will be added later
{ cat LICENSE; echo ""; } >> "${bin}"

sed -i -e 's:^:# :g' -e 's:# !:#!:g' "${bin}"

included=""

# This fn copies the file into bin after copying is includes
copy() {
    for i in ${included}; do
        if [ "${i}" = "${1}" ]; then
            return 0
        fi
    done
    grep -E "\. \./.+\.sh" "src/${1}" | while read -r include; do
        ifile=$(echo "${include}" | awk '{print $2}')
        copy "${ifile}"
    done
    cat "src/${1}" >> "${tmp}"
    included="${included} ${1}"
}
log "Merging source files into ${tmp}" info
copy "${MYNAME}"

# Cleanup includes, comments and empty lines
log "Removing comments and empty lines" info
sed -i -e 's:^\. \./.*\.sh$::g' -e 's:^#.*$::g' -e '/^[[:space:]]*$/d' "${tmp}"

log "Creating final script" info
cat "${tmp}" >> "${bin}"
rm "${tmp}"
chmod 775 "${bin}"

log "Creating release files"
release="${MYNAME}-${MYVERSION}"
tar -C ./dist -cf "./dist/${release}.tar" "./${MYNAME}"
(cd dist && sha256sum --tag "${release}.tar" > "${release}-CHECKSUM")
gpg --clearsign "./dist/${release}-CHECKSUM"
mv "./dist/${release}-CHECKSUM.asc" "./dist/${release}-CHECKSUM"

log "${MYNAME} ${MYVERSION} is ready" success
