#--
# Copyright (c) 2021 Hugo Rodrigues
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#++

. ./system.sh
. ./variables.sh

createfs_alpine() {
    local output="${1}"
    local release="${2}"
    local arch="${3}"

    [ -z "${release}" ] && release="latest-stable"

    local sha="sha256sum"
    if ! checkbin "${sha}"; then
        sha="sha256"
    fi
    local check_gpg=1

    requirebin awk grep "${sha}" gzip tail head

    local mirror="https://dl-cdn.alpinelinux.org/alpine/${release}/releases/${arch}"
    local sha_args="--quiet"
    if [ "${VERBOSE}" -eq 2 ]; then
        sha_args=""
    fi

    log "Importing Alpine Linux GPG key" info
    gpg_import "https://alpinelinux.org/keys/ncopa.asc" "${GPG_ALPINE_RELEASE_FINGERPRINT}"

    log "Fetching last release for ${release}" info
    download "${mirror}/latest-releases.yaml" "${TEMPDIR}/latest-releases.yaml" || die "Unable to download release information" "${EXITC_DOWNLOAD_ERROR}"
    local file=$(grep "file: alpine-minirootfs" "${TEMPDIR}/latest-releases.yaml" | awk '{print $2}')

    if [ -z "${file}" ]; then
        die "Alpine Linux ${release} doesn't provide a rootfs" "${EXITC_INVALID_ARG}"
    fi

    log "Download rootfs from Alpine Linux repository" info
    for ext in "" ".asc" ".sha256"; do
        local invalid=0
        log "${mirror}/${file}${ext}" debug
        download "${mirror}/${file}${ext}" "${TEMPDIR}/${file}${ext}" || invalid=1
        if [ "${invalid}" -eq 1 ]; then
            if [ "${ext}" = ".asc" ]; then
                check_gpg=0
                log "Alpine Linux doesn't provide a GPG signature for ${file}. Use a your own risk" warning
            else
                die "Unable to download ${mirror}/${file}${ext}" "${EXITC_DOWNLOAD_ERROR}"
            fi
        fi
    done

    log "Verifying files" info
    (cd "${TEMPDIR}" && ${sha} ${sha_args} -c "${file}.sha256") || die "Invalid sha256 checksum for ${file}" "${EXITC_VALIDATION_ERROR}"

    [ "${check_gpg}" -eq 1 ] && gpg_verify "${TEMPDIR}/${file}.asc" "${TEMPDIR}/${file}"

    gzip --decompress "${TEMPDIR}/${file}"
    mv "${TEMPDIR}/${file//.gz/}" "${output}"
}
