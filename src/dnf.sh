#--
# Copyright (c) 2021 Hugo Rodrigues
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#++

. ./log.sh
. ./system.sh

createfs_dnf() {
    local rootfs="${1}"
    local distro="${2}"
    local release="${3}"
    local arch="${4}"
    local mirrorlist=""
    local gpgkey=""

    iamroot
    requirebin dnf

    case "${distro}" in
        centos)
            [ -z "${release}" ] && release="8"
            mirrorlist="http://mirrorlist.centos.org/?release=${release}&arch=${arch}&repo=BaseOS";
            mirror=""
            gpgkey="https://www.centos.org/keys/RPM-GPG-KEY-CentOS-Official";;
        rocky)
            [ -z "${release}" ] && release="8"
            mirrorlist=""
            mirror="http://dl.rockylinux.org/pub/rocky/${release}/BaseOS/${arch}/os";
            gpgkey="https://dl.rockylinux.org/pub/rocky/RPM-GPG-KEY-rockyofficial";;
        fedora)
            [ -z "${release}" ] && release="34"
            mirror=""
            mirrorlist="https://mirrors.fedoraproject.org/mirrorlist?repo=fedora-${release}&arch=${arch}";
            gpgkey="https://getfedora.org/static/fedora.gpg";;
    esac

    log "Creating rootfs for ${distro} ${release} on ${arch}" info
    log "Using ${mirrorlist} as mirror list for dnf" debug
    log "Using ${gpgkey} as gpgkey for dnf" debug

    local repofile="${TEMPDIR}/repo"
    cat << EOF > "${repofile}"
[base]
name=base
mirrorlist=${mirrorlist}
baseurl=${mirror}
gpgcheck=1
enabled=1
gpgkey=${gpgkey}
EOF

    local dnfargs=""
    case "${VERBOSE}" in
        0) dnfargs="${dnfargs} --quiet";;
        2) dnfargs="${dnfargs} --verbose";;
    esac

    dnf ${dnfargs} --config="${repofile}" --forcearch="${arch}" --releasever="${release}" --disablerepo=* --enablerepo=base --installroot="${rootfs}" group install -y 'Minimal Install'
}
