#--
# Copyright (c) 2021 Hugo Rodrigues
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#++

. ./log.sh
. ./variables.sh

die() {
    local message="${1}"
    local exitc="${2}"

    log "${message}" error
    exit "${exitc}"
}

checkbin() {
    local out=0
    for bin in "${@}"; do
        local loc=$(command -v "${bin}")
        if [ -z "${loc}" ]; then
            log "${bin} not found" warning
            out=1
        else
            log "${bin} is ${loc}" debug
        fi
    done
    return "${out}"
}

requirebin() {
    checkbin "${@}" || die "Some required binaries are missing. Please install them in order to continue" "${EXITC_MISSING_BIN}"
}

iamroot() {
    [ "$(id -u)" -eq 0 ] || die "${MYNAME} requires root to create this rootfs" "${EXITC_REQUIRE_ROOT}"
}

download() {
    local url="${1}"
    local dest="${2}"

    requirebin curl

    local curl_args="--silent"
    if [ "${VERBOSE}" -eq 3 ]; then
        curl_args="--verbose"
    fi

    log "Downloading ${url} to ${dest}" debug
    curl ${curl_args} --location --fail "${url}" -o "${dest}" || return 1
    return 0
}

_gpg() {
    if [ "${GPG_ENABLE}" -ne 1 ]; then
        return 0
    fi
    local gpg="gpg2"
    if ! checkbin "${gpg}"; then
        gpg="gpg"
    fi
    requirebin "${gpg}"
    local gpg_args="--quiet"
    if [ "${VERBOSE}" -eq 3 ]; then
        gpg_args="--verbose"
    fi
    ${gpg} ${gpg_args} "${@}"
    return "${?}"
}

gpg_check_fingerprint() {
    if [ "${GPG_ENABLE}" -ne 1 ]; then
        return 0
    fi
    local key="${1}"
    local expected_fingerprint="${2}"
    local fingerprint=$(_gpg --fingerprint --show-key "${keyf}" | head -n +2 | tail -n 1 | sed 's:^[[:space:]]*::g')
    [ "${fingerprint}" = "${expected_fingerprint}" ] && return 0
    return 1
}

gpg_import() {
    if [ "${GPG_ENABLE}" -ne 1 ]; then
        return 0
    fi
    local key="${1}"
    local expected_fingerprint="${2}"

    requirebin grep

    local ishttp=$(echo "${key}" | grep "://")
    local keyf="${key}"
    if [ -n "${ishttp}" ]; then
        keyf=$(mktemp --tmpdir="${TEMPDIR}")
        download "${key}" "${keyf}" || die "Unable to download GPG key from ${key}" "${EXITC_DOWNLOAD_ERROR}"
    fi
    if [ -n "${expected_fingerprint}" ]; then
        gpg_check_fingerprint "${keyf}" "${expected_fingerprint}" || die "GPG fingerprint doesn't match" "${EXITC_VALIDATION_ERROR}"
    fi
    _gpg --import "${keyf}" || die "Unable to import GPG key ${key}" "${EXITC_VALIDATION_ERROR}"
}

gpg_verify() {
    if [ "${GPG_ENABLE}" -ne 1 ]; then
        return 0
    fi
    local signature="${1}"
    local tocheck="${2}"
    _gpg --trust-model always --verify "${signature}" "${tocheck}" || die "Invalid GPG signature for ${tocheck}" "${EXITC_VALIDATION_ERROR}"
}

gpg_verify_message() {
    if [ "${GPG_ENABLE}" -ne 1 ]; then
        return 0
    fi
    local tocheck="${1}"
     _gpg --trust-model always --verify "${tocheck}" || die "Invalid GPG signature for ${tocheck}" "${EXITC_VALIDATION_ERROR}"
}
