#--
# Copyright (c) 2021 Hugo Rodrigues
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#++

MYVERSION="1.1.1.0"
MYNAME="mkrootfs"

VERBOSE=1
TEMPDIR="$(mktemp -d)"
GNUPGHOME="${TEMPDIR}/gpg"
export GNUPGHOME
mkdir -p "${GNUPGHOME}"
chmod 700 "${GNUPGHOME}"

GPG_ENABLE=1

EXITC_INVALID_ARG=1
EXITC_MISSING_BIN=2
EXITC_FILE_EXISTS=3
EXITC_INVALID_DISTRO=4
EXITC_REQUIRE_ROOT=5
EXITC_INVALID_COMPRESSION=6
EXITC_COMPRESSION_ERROR=7
EXITC_VALIDATION_ERROR=8
EXITC_DOWNLOAD_ERROR=9
EXITC_DEB_ERROR=10

# Used to validate the gpg key download from the interweb
GPG_ALPINE_RELEASE_FINGERPRINT="0482 D840 22F5 2DF1 C4E7  CD43 293A CD09 07D9 495A"
