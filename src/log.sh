#--
# Copyright (c) 2021 Hugo Rodrigues
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#++

# Logging tools

error() {
    printf "\033[0;31m%s\033[m\n" "${1}"
}

warning() {
    printf "\033[1;33m%s\033[m\n" "${1}"
}

info() {
    if [ "${VERBOSE}" -gt 0 ]; then
        printf "\033[0;36m%s\033[m\n" "${1}"
    fi
}

debug() {
    if [ "${VERBOSE}" -eq 2 ]; then
        printf "%s\n" "${1}"
    fi
}

success() {
    if [ "${VERBOSE}" -gt 0 ]; then
        printf "\033[0;32m%s\033[m\n" "${1}"
    fi
}

log() {
    local message=${1}
    local level=${2:-"info"}
    local log_format="$(date +"%Y-%m-%d %H:%M:%S") $(echo "${level}" | awk '{print toupper($0)}'): "
    local log_message=${log_format}${message}
    

    case ${level} in
        info) info "${log_message}" ;;
        warning) warning "${log_message}" ;;
        error) error "${log_message}" ;;
        success) success "${log_message}" ;;
        debug) debug "${log_message}" ;;
        *) info "${log_message}" ;;
    esac
}
