#--
# Copyright (c) 2021 Hugo Rodrigues
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#++

. ./log.sh
. ./system.sh

download_deb() {
    local mirror="${1}"
    local package="${2}"
    local version="${3}"
    local arch="${4}"
    local tmp="${TEMPDIR}/${package}"
    local html="${tmp}/download.html"
    local durl=""
    
    requirebin ar
    
    mkdir "${tmp}"
    download "${mirror}/${version}/${arch}/${package}/download" "${html}" || die "Unable to download ${package} download link" "${EXITC_DOWNLOAD_ERROR}"
    durl=$(grep -e 'href="http.*\.deb"' "${html}" | head -n 1 | cut -d'"' -f2)
    download "${durl}" "${tmp}/package.deb" || die "Unable to download ${package}" "${EXITC_DOWNLOAD_ERROR}"
    (cd "${tmp}"; ar x "package.deb") || die "Unable to unpack ${package}" "${EXITC_COMPRESSION_ERROR}"

    if [ -f "${tmp}/data.tar.xz" ]; then
        requirebin xz
        xz --decompress "${tmp}/data.tar.xz"
    elif [ -f "${tmp}/data.tar.gz" ]; then
        requirebin gzip
        gzip --decompress "${tmp}/data.tar.gz"
    elif [ ! -f "${tmp}/data.tar" ]; then
        die "Unable to unpack data.tar from ${package}" "${EXITC_COMPRESSION_ERROR}"
    fi

    mv "${tmp}/data.tar" "${TEMPDIR}/${package}.tar"
}

createfs_deb() {
    local rootfs="${1}"
    local distro="${2}"
    local release="${3}"
    local arch="${4}"
    local keyring="${TEMPDIR}/usr/share/keyrings/${distro}-archive-keyring.gpg"
    local debootstrap="${TEMPDIR}/usr/sbin/debootstrap"
    local keymirror=""
    local keypackage=""

    # Fix arch
    [ "${arch}" = "x86_64" ] && arch="amd64"
    local debootstrap_args="--arch=${arch}"

    iamroot
    requirebin wget gzip grep sed

    DEBOOTSTRAP_DIR="${TEMPDIR}/usr/share/debootstrap"
    export DEBOOTSTRAP_DIR

    case "${distro}" in
        debian)
            [ -z "${release}" ] && release="stable";
            keymirror="https://packages.debian.org";
            keypackage="debian-archive-keyring";;
        ubuntu)
            [ -z "${release}" ] && release="focal";
            keymirror="https://packages.ubuntu.com";
            keypackage="ubuntu-keyring";;
    esac

    log "Downloading debootstrap" info
    download_deb "https://packages.debian.org" "debootstrap" "sid" "all"
    tar -C "${TEMPDIR}" -xf "${TEMPDIR}/debootstrap.tar" || die "Unable to unpack debootstrap" "${EXITC_COMPRESSION_ERROR}"

    if [ "${GPG_ENABLE}" -eq 1 ]; then
        requirebin gpgv
        log "Downloading keyrings" info
        download_deb "${keymirror}" "${keypackage}" "${release}" "all"
        tar -C "${TEMPDIR}" -xf "${TEMPDIR}/${keypackage}.tar" || die "Unable to unpack ${keypackage}" "${EXITC_COMPRESSION_ERROR}"
        debootstrap_args="${debootstrap_args} --keyring=${keyring}"
    else
        debootstrap_args="${debootstrap_args} --no-check-gpg"
    fi

    log "Creating rootfs for ${distro} ${release} on ${arch}" info
    ${debootstrap} ${debootstrap_args} "${release}" "${rootfs}"

}
